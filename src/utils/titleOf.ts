export default function getTitle(document: Document) {
  const { textContent } = document.getElementById('FDFXFA_PDFName')
  return textContent.split('.')[0]
}
