import { promises as fsp } from 'fs'

export default async function saveTo(filename: string, data: Object) {
  await fsp.writeFile(filename, JSON.stringify(data, undefined, 2))
}
