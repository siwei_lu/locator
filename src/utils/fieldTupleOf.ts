import { FieldTuple, Field } from '../types'
import coordinateOf from './coordinateOf'
import { offset } from '../constants'

const regExpOfInput = /[pP]\D*(\d+)\D*L\D*(\d*)([a-z]*)[^A-Z]*([A-Z][a-zA-Z]*)?.*\]$/
const regExpOfCheckbox = /[pP]\D*(\d+)\D*(\d*)([a-z]*)/

export default function fieldTupleOf(el: HTMLInputElement) {
  return global.app.withFieldName
    ? fieldTupleNamedInFieldName(el)
    : fieldTupleNamedInKey(el)
}

function resultOfCheckbox(el: HTMLInputElement) {
  const result = el.getAttribute('data-field-name').match(regExpOfCheckbox)

  if (result == null) {
    return undefined
  }

  const name = result
    .slice(1)
    .filter(s => s)
    .join('.')
  return { name, content: 'x' }
}

function resultOfInput(el: Element) {
  const result = el.getAttribute('data-field-name').match(regExpOfInput)

  if (result == null) {
    return undefined
  }

  const content = result.pop()
  const name = result
    .slice(1)
    .filter(s => s)
    .join('.')

  return { name, content }
}

function fieldTupleNamedInFieldName(el: HTMLInputElement): FieldTuple {
  const { left, top, height } = global.window.getComputedStyle(el)
  const x = coordinateOf(left) + offset.checkbox.x
  const y = 800 - coordinateOf(top) - coordinateOf(height)

  const result =
    el.type === 'checkbox' ? resultOfCheckbox(el) : resultOfInput(el)

  if (result == null) {
    return undefined
  }

  const { name, content } = result
  const field: Field = {
    content: content || 'default',
    pos: [x, y],
    rule: ''
  }

  return [name, field]
}

function fieldTupleNamedInKey(el: HTMLInputElement): FieldTuple {
  const { left, top, height } = global.window.getComputedStyle(el)
  const x = coordinateOf(left) + offset.checkbox.x
  const y = 800 - coordinateOf(top) - coordinateOf(height)

  const title = el.getAttribute('title')

  const [, number, section] = el.id.match(/(\d+)_(\d+)$/)
  const name = [section, number].join('.')

  const content =
    el.type === 'checkbox'
      ? 'x'
      : el
          .getAttribute('data-field-name')
          .split('.')
          .pop()

  const field: Field = {
    content: content || 'default',
    pos: [x, y],
    rule: '',
    title
  }

  return [name, field]
}
