export interface Field {
  pos: [number, number]
  content: string
  rule: ''
  title?: string
}

export type FieldTuple = [string, Field]
