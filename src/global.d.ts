declare type Command = import('commander').Command

declare module NodeJS {
  interface Global {
    app: Command
  }
}
