import * as app from 'commander'
import generate from './utils/generate'
import saveTo from './utils/saveTo'

export default function main() {
  Object.assign(global, { app })
  app
    .option('-w --with-field-name', 'generate json file with fieldname as key')
    .arguments('<path>')
    .action(async (path: string) => {
      const { title, value } = await generate(path)
      await saveTo(`${title}.json`, value)
    })
    .parse(process.argv)
}
