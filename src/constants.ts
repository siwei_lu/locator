export const ratio = 792 / 1210

export const offset = {
  input: {
    x: 3,
    y: 6
  },
  checkbox: {
    x: 2,
    y: 8
  }
}
