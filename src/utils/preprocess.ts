export default function preprocess(document: Document) {
  document
    .querySelectorAll('input[type="checkbox"]')
    .forEach((el: HTMLInputElement) => {
      Object.assign(el.style, { display: 'block', visibility: 'hidden' })
    })
}
