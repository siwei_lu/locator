import preprocess from './preprocess'
import getResult from './getResult'
import getTitle from './titleOf'
import createDocument from './document'

export default async function generate(path: string) {
  const document = await createDocument(path)

  preprocess(document)
  const value = getResult(document)
  const title = getTitle(document)

  return { title, value }
}
