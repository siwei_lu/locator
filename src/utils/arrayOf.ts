interface Payload<T> {
  forEach(cb: (item: T) => void): void
}

export default function arrayOf<T>(list: Payload<T>) {
  const arr: T[] = []
  list.forEach(i => arr.push(i))
  return arr
}
