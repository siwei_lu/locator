# README

A locator to locate all fields you need in a worksheet.

## Install Globally

```sh
npm install -g https://bitbucket.org/siwei_lu/locator
```

## Usage

### Generate a JSON file by using elements' idx as keys

```sh
locator /path/to/converted/form.html
```

### Generated a JSON file by using field names as keys

##### NOTE: We cannot make sure all elements can be catched in this mode

```sh
locator -w /path/to/converted/form.html
```
