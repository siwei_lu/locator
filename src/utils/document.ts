import { promises as fsp } from 'fs'
import { JSDOM } from 'jsdom'

export default async function createDocument(path: string) {
  const html = await fsp.readFile(path, { encoding: 'utf-8' })
  const { window } = new JSDOM(html)

  Object.assign(global, { window })
  return window.document
}
