import { ratio } from '../constants'

export default function coordinateOf(pixel: string) {
  return Math.round(parseInt(pixel) * ratio)
}
