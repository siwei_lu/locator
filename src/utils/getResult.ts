import arrayOf from './arrayOf'
import fieldTupleOf from './fieldTupleOf'
import { FieldTuple } from '../types'

function mergedBySameKey(tuples: FieldTuple[]): FieldTuple[] {
  const sum = {}

  tuples.forEach(([k, v]) => {
    if (sum[k] == null) {
      sum[k] = v
      return
    }

    if (sum[k] instanceof Array) {
      sum[k].push(v)
      return
    }

    sum[k] = [sum[k], v]
  })

  return Object.entries(sum)
}

function expandedList(tuples: FieldTuple[]): FieldTuple[] {
  const sum = []

  tuples.forEach(t => {
    const [name, field] = t

    if (field instanceof Array) {
      field.forEach((v, i) => sum.push([`${name}.${i + 1}`, v]))
    } else {
      sum.push(t)
    }
  })

  return sum
}

function resultOfPage(page: Element) {
  const elements = arrayOf(page.querySelectorAll('[data-field-name]'))
  const tuples = mergedBySameKey(elements.map(fieldTupleOf).filter(s => s))

  return Object.fromEntries(expandedList(tuples))
}

export default function resultOf(document: Document) {
  const tuples = arrayOf(document.querySelectorAll('.pageArea')).map((p, i) => [
    `page${i + 1}`,
    resultOfPage(p)
  ])

  return Object.fromEntries(tuples)
}
